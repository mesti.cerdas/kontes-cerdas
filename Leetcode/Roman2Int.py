num = {
	"I" : 1,
	"V" : 5,
	"X" : 10,
	"L" : 50,
	"C" : 100,
	"D" : 500,
	"M" : 1000
}

res = 0
s = input()
for i, j in enumerate(s[:-1]) :
	if num[j] >= num[s[i+1]] :
		res += num[j]
	else :
		res -= num[j]
res += num[s[-1]]
print(res)