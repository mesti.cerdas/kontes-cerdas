// Author : LoGic b0ys
// Date   : 2016-08-09

/*************************************************************************************** 
 *
 * Fungsi untuk menambahkan bilangan tanpa operator + atau -
 * Menggunakan prinsip adder
 * Carry dengan and
 * Hasil dengan xor
 ***************************************************************************************/

class Solution {
public:
    int getSum(int a, int b) {
        while(b != 0){
        	int c = a&b;
        	a = a^b;
        	b = c << 1;
        }
        return a;
    }
};
