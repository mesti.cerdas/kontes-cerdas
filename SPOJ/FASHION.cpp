#include<iostream>
#include<algorithm>

using namespace std;

int m[1005], w[1005];

int main(){
	int t;
	cin >> t;
	while(t--){
		int n, res = 0;
		cin >> n;
		for(int i = 0; i < n; i++) cin >> m[i];
		for(int i = 0; i < n; i++) cin >> w[i];
		sort(m, m + n);
		sort(w, w + n);
		for(int i = 0; i < n; i++) res += m[i]*w[i];
		cout << res << endl;
	}
}
