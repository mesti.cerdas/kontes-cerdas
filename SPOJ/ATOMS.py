from math import floor, log
p = int(input())
while(p > 0) :
    n,k,m = input().split()
    n = int(n)
    k = int(k)
    m = int(m)
    if(m <= n) :
        print(0)
    else :
        print(floor(log(m/n,10)/log(k,10)))
    p = p-1
