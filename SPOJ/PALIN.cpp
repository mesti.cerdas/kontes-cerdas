/* 
 * Problem PALIN
 * Author : LoGic_b0ys
 * Ad-Hoc
 * Problem kali ini untuk mencari bilangan palindrom selanjutnya dari bilangan yang diberikan
 * Untuk mencari bilangan tersebut, kita lihat dari setengah sebelah kanan (dari yang paling kecil)
 * Nah...! Jika ada salah satu bilangan saja dari setengah sebelah kiri yang lebih kecil dari sebelah kanan
 * Maka bilangan tengah akan ditambah 1
 * Untuk melakukannya bilangan akan dimasukkan dalam string dan diconvert menjadi array of int
 */

#include<iostream>
#include<stdio.h>
#include<string>
#include<cstring>

using namespace std;

int *num;
int r,z=0;

int increase(){ //fungsi increase untuk menambahkan beberapa nomor yang disimpan di array dan lebih dari 10 (intinya teknik menyimpan)
	int sub;
	
	for (sub = r/2; sub < r; sub++){
		if (num[sub] >= 10) {
			num[sub]=0;
			num[sub + 1]++;
			
			if(sub==r-1){ //jika ternyata ada juga yang masih tersimpan di bilangan paling besar, maka bilangan bertambah panjang 1 angka
				z=1;
			}
		}
		else break;
	}
}

int palin() {
    int q,m;
    
    for(q=0;q<r/2;q++){ //untuk mencari setengah bagian kiri
    	
        if(num[r/2-1-q]>=num[(r+1)/2+q]){ //jika ada bilangan bagian kiri yang lebih besar daripada bagian kanan
        	
            if((num[r/2-1-q]>num[(r+1)/2+q])||(q==r/2-1)){ //untuk membatasi apabila bilangannya sama
	            num[r/2]++; //syarat ini akan berlaku jika bilangan berada tepat di sebelah kanan bilangan tengah
	            break;
			}
			
        }
        else break;
    }
    
    increase(); //lakukan teknik menyimpan
    if(z) r++;
    
    for(q=0;q<r/2;q++) num[r/2-1-q]=num[(r+1)/2+q]; //copy setengah kanan ke setengah kiri
}

int main() {
    int t;
    cin >> t;
    
    while (t--) {
        getchar();
        int inc=0, sub, m, q, a;
        num = new int[1000001];
        string c;
        cin>>c;
        
        z=0;
        r = c.size();
            
        for (q = r - 1, a = 0; q >= 0; q--, a++) { //ini akan membalik bilangan
            num[a] = c[q] - 48; //convert dari string menuju integer
        }
        if(r==1) num[0]++; //jika bilangan hanya satu angka maka langsung increase aja

        palin();
            
        for (m = r-1 ; m >= 0; m--) cout << num[m];
        cout << endl;
    }
    return 0;
}
