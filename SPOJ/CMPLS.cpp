/*
 * Ad-Hoc Mas
 * Saya kok gak kepikiran sama sekali si -_-
 * Inti dari masalah ini adalah complete the sequence dari sebuah series bilangan
 * Dan cara yang digunakan adalah barisan aritmetika berderajat tinggi
 * Visualisainya :
 	Untuk x^2
 	1 4 9 16 25 36
 	\/\/\/ \/ \ / 
 	3 5 7  9  11
 	\/\/\ / \/
 	2 2  2  2
 * Untuk polinom berderajat 2 maka beda akan konstan pada tingkat kedua
 * Jika kita ingin mengetahui pola selanjutnya, maka tinggal menjumlahkan bagian akhir keatas
 * 2 + 11 + 36 = 49
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <climits>

using namespace std;

#define DEBUG 0
#define gc getchar
#define ll long long

int main(int argc, char * argv[]) {

	int t;
	scanf("%d", &t);

	while (t--) {

		vector<int> last_val;
		vector<int> diff;

		int d = -1;	// difference

		int n, k;
		int flag = 0;

		scanf("%d%d", &n, &k);

		int temp, temp_old;

		for (int i = 0; i < n; i++) {

			scanf("%d", &temp);

			if (i != 0) {
				d = temp - temp_old;

				if (flag == 0) 
					flag = d; //jika flag = 0 itu artinya ada elemen yang sama, maka harus diubah

				diff.push_back(d); //diff berisi selisih antara elemen berurutan

			}

			if (i == n - 1) {
				last_val.push_back(temp);
				last_val.push_back(d);
				//last_val berisi nilai akhir dan juga selisih dari nilai akhir dan nilai sebelum akhir
			}

			temp_old = temp;

		}
		//jika bilangan hanya ada satu maka pola tetap (konstan)
		if (n == 1) {
			for (int i = 0; i < k; i++) {
				//if (i != 0)
				//	printf(" ");
				printf("%d ", last_val[0]);
			}
			continue;
		}

		d = -1;

		while (flag != 0) { //ketika flag masih 0 bahkan setelah diubah berarti barisnya konstan
		//jadi while yang ini mencari selisih dari tiap selisih dari sequence
		//perulangan akan berhenti jika selisih ini memberi nilai konstan
		//artinya tiap selisih sudah mencapai nilai 0

			int temp_flag = 0; //temp_flag digunakan untuk menandai apakah beda sudah mencapai konstan

			for (int i = 0; i < diff.size() - 1; i++) { //fonr ini untuk mencari selisih diantara selisih

				d = diff[i + 1] - diff[i]; 

				if (temp_flag == 0)
					temp_flag = d;

				diff[i] = d;

				if (i == diff.size() - 2)
					last_val.push_back(d); //lalu dimasukkan dalam last_val untuk disimpan sebagai beda

			}

			diff.pop_back();

			if (temp_flag == 0) //jika sudah mencapai nilai konstan maka keluar dari perulangan
				flag = 0;

			if (diff.size() == 1) //jika selisih hanya ada 1 berarti sudah konstan
				flag = 0;

		}

		if (last_val[last_val.size() - 1] == 0) //jika nilai variabel akhir sama dengan 0 maka ambil variabel itu
			last_val.pop_back();

#if DEBUG
		cout << "DIFF Vector : ";
		for (vector<int>::iterator itr = diff.begin(); itr != diff.end();
				itr++) {
			cout << *itr << " ";
		}
		cout << endl;
		cout << "LAST VAL Vector : ";
		for (vector<int>::iterator itr = last_val.begin();
				itr != last_val.end(); itr++) {
			cout << *itr << " ";
		}
		cout << endl;
#endif

		for (int i = 0; i < k; i++) {
			for (int j = last_val.size() - 2; j >= 0; j--) {
				last_val[j] = last_val[j] + last_val[j + 1]; //menambahkan tiap beda dari barisan aritmetika berderajat tinggi
			}
			//if (i != 0)
			//	printf(" ");
			printf("%d ", last_val[0]); //last_val[0] yang bernilai variabel terakhir akan ditambah dengan semua bedanya
			//ini akan berlangsung selama k kali dan akan membentuk sebuah barisan aritmetika (nanti coba saya visualisasikan)
		}
		printf("\n");

	}
	return 0;
}
