#include<iostream>
#include<stdio.h>
#include<cstring>
#include<algorithm>

using namespace std;

int memo[1000005];

long long solve(long long n){
	if(n <= 1000000){
		if(memo[n] != -1)
			return memo[n];
		memo[n] = max(n, solve(n/4)+solve(n/3)+solve(n/2));
		return memo[n];
	} else 
		return max(n, solve(n/4)+solve(n/3)+solve(n/2));
}

int main(){
	memset(memo, -1, sizeof(memo));
	memo[0] = 0;
	memo[1] = 1;
	memo[2] = 2;
	memo[3] = 3;
	long long n;
	while(scanf("%lld", &n) != -1){
		cout << solve(n) << endl;
	}
	return 0;
}