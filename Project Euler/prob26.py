from Euler import prime_sieve

n = prime_sieve(1000) #list all prime below 1000
for i in n[::-1] : #we count in backward
    c = 1
    while pow(10, c, i) != 1 : c += 1 #pow function is for 10^c mod i (fast power modullo)
    if i-1 == c : break #1/d when d is prime have cycle up to d-1
    #so if c = d-1 we have max number cycle
    #remember, we count it backward so it's also the largest prime number
print(i)
