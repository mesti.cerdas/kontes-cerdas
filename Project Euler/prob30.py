from Euler import pow_digits
e = 5
L = 9**e * (e-1)
print(sum(n for n in range(10, L) if pow_digits(n, e) == n) #simply bruteforce)
