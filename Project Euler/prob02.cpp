#include<iostream>

using namespace std;

int main(){
	int fib[100];
	fib[0] = 1;
	fib[1] = 1;
	int res = 0;
	for(int i = 2; ; i++){
		fib[i] = fib[i-1] + fib[i-2];
		if(fib[i]%2 ==0) res += fib[i];
		if(fib[i] > 4000000) break;
	}
	cout << res << endl;
	return 0;
}
