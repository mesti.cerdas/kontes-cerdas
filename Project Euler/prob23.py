from Euler import d #return proper divisors for n

L, s = 20161, 0

abn = set() #set of abundant number

for n in range (1, L+1) :
    if d(n) > n :
        abn.add(n)
    if not any((n-a in abn) for a in abn) : #if n doesn't have abundant pairing in the abn set
        #we can count it as integer which cannot be written as the sum of two numbers
        s+=n

print(s)
