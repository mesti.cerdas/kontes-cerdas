from Euler import prime_sieve, is_prime

L = 1000
nmax = 0
for b in prime_sieve(L):
    for a in range(-b+2, 0, 2):
        n = 1
        while is_prime(n*n + a*n + b): n+= 1 #simply brute force
        if n>nmax: nmax, p = n, (a,b)
print(p[0]*p[1])
