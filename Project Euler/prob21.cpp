#include<iostream>
#include<cstring>

using namespace std;

int prime[1250];

void generatePrime(){
	prime[0] = 2;
	prime[1] = 3;
	int jml = 2;
	for(int i=5; i < 10000; i+=2)
		for(int j = 3; j <= i; j++)
			if(i==j) prime[jml++] = i; else if(!(i%j)) break;
}

int sumProp(int bil){
        int n = bil;
	int pos = 0, pDiv = prime[pos++];
	int res = 1, div = pDiv;
	while(n != 1){
		int tmp;
		if(!(n%div)) tmp = 1; else tmp = 0;
		while(!(n%pDiv)){
			tmp += pDiv;
			pDiv = pDiv*div;
		}
		if(tmp != 0 ){
			pDiv = pDiv/div;
//			cout << n << " / " << pDiv << endl;
			n = n/pDiv;
			res = res*tmp;
		}
		div = pDiv = prime[pos++];
	}
	return res - bil;
}

int main(){
	generatePrime();
	int res = 0;
	for(int i = 2; i < 10000; i++){
		int a = sumProp(i);
//		cout << i << endl;
		if(a <= 10000 && sumProp(a) == i && a != i) {
			cout << i << endl;
			res += i;
		}
	}
	cout << "The Result is : " << res << endl;
	return 0;
}
