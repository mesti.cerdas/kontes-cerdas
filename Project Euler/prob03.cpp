#include<iostream>
#include<cstring>

using namespace std;

bool sieve[7000];

void generate(){
	memset(sieve, true, sizeof(sieve));
	for(int i = 2; i < 7000; i++){
		if(!sieve[i]) continue;
		for(int j = 2*i; j < 7000; j+=i) sieve[j] = false;
	}
}

int main(){
	generate();
	long long bil = 600851475143;
	for (int i = 2; i < 7000; i++) if(sieve[i] && bil%i == 0) cout << i << " ";
	cout << endl;
	return 0;
}
