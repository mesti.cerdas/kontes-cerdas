file = open("prob22.txt", "r");
s = file.read().rstrip();
l = s.split(",")
l.sort()
res  = 0
for ln, name in enumerate(l, start=1) :
    res += ln * sum(ord(c)-64 for c in name)
print(res)
