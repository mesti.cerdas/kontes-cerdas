#include<cstdio>

using namespace std;

int main(){
	int n, count = 1;
	do{
		scanf("%d", &n);
		if(!n) continue;
		int re = 0, tr = 0;
		while(n--){
			int a;
			scanf("%d", &a);
			if(a)re++; else tr++;
		}
		printf("Case %d: %d\n", count, re-tr);
		count++;
	} while(n != 0);
	return 0;
}
