#include<cstdio>
#include<algorithm>

using namespace std;

int main(){
	int data[3], n;
	scanf("%d", &n);
	for(int i = 0; i < n; i++){
		scanf("%d %d %d", &data[0], &data[1], &data[2]);
		sort(data, data+3);
		printf("Case %d: %d\n",i+1, data[1]);
	}
}
