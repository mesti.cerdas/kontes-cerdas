#include<cstdio>
#include<cmath>

using namespace std;

int main(){
	int t; scanf("%d", &t);
	while(t--){
		int m, n;
		scanf("%d %d", &m, &n);
		if(m > n) puts(">"); else if(m < n) puts("<"); else puts("=");
	}
	return 0;
}
