#include<cstdio>

using namespace std;

int main(){
	int k;
	while(scanf("%d", &k) != 0){
		if(k == 0) break;
		int m, n; scanf("%d %d", &n, &m); //n = x, m = y
		while(k--){
			int x, y; scanf("%d %d", &x, &y);
			if(x < n && y < m) printf("SO");
			else if(x > n && y < m) printf("SE");
			else if(x < n && y > m) printf("NO");
			else if(x > n && y > m) printf("NE");
			else printf("divisa");
			printf("\n");
		}
	}
}