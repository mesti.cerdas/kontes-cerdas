#include<cstdio>

using namespace std;

int main(){
	int t; scanf("%d", &t);
	while(t--){
		int n; scanf("%d", &n);
		int min = 105, max = 0;
		while(n--){
			int b; scanf("%d", &b);
			if(min > b) min = b;
			if(max < b) max = b;
		}
		printf("%d\n", 2*(max-min));
	}
}