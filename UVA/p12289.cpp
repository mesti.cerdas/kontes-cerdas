#include<iostream>
#include<string>

using namespace std;

int main(){
	int n; cin >> n;
	while(n--){
		int count = 0;
		string h; cin >> h;
		if(h.size() > 3) printf("3\n");
		else {
			if(h[0] == 'o') count++;
			if(h[1] == 'n') count++;
			if(h[2] == 'e') count++;
			if(count > 1){
				printf("1\n");
				continue;
			}
			printf("2\n");
		}
	}
}
