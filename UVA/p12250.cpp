#include<iostream>
#include<cstdio>

using namespace std;

int main(){
	int count = 1;
	string s;
	do{
		cin >> s;
		if(s == "#") continue;
		if(s == "HELLO") printf("Case %d: ENGLISH\n", count);
		else if(s == "HOLA") printf("Case %d: SPANISH\n", count);
		else if(s == "HALLO") printf("Case %d: GERMAN\n", count);
		else if(s == "BONJOUR") printf("Case %d: FRENCH\n", count);
		else if(s == "CIAO") printf("Case %d: ITALIAN\n", count);
		else if(s == "ZDRAVSTVUJTE") printf("Case %d: RUSSIAN\n", count);
		else printf("Case %d: UNKNOWN\n", count);
		count++;
	} while(s != "#");
	return 0;
}
