#include<cstdio>
#include<cmath>

using namespace std;

int main(){
	int k; scanf("%d", &k);
	while(k--){
		int n; scanf("%d", &n);
		int res = abs((int)((315*n+36962)/10)%10);
		printf("%d\n", res);
	}
}