#include<iostream>
#include<cstdio>
#include<string>

using namespace std;

int main(){
	int t, res = 0; scanf("%d", &t);
	while(t--){
		string s; cin >> s;
		if(s == "donate"){
			int a; cin >> a;
			res += a;
		} else if(s == "report") cout << res << endl;
	}
	return 0;
}