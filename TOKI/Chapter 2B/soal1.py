inp = [int(x) for x in input().split()]
matriks =[]
for x in range(inp[0]) :
    matriks.append([int(x) for x in input().split()])
matriks = list(zip(*matriks[::-1]))
res = [list(x) for x in matriks]
print('\n'.join([''.join([str(item) + " " if item != row[-1] else str(item) for item in row]) for row in res]))
