def matmult(a,b):
    zip_b = zip(*b)
    zip_b = list(zip_b)
    return [[sum(ele_a*ele_b for ele_a, ele_b in zip(row_a, col_b)) for col_b in zip_b] for row_a in a]

inp = [int(x) for x in input().split()]
matriks1 = []
for x in range(inp[0]) :
    matriks1.append([int(x) for x in input().split()])
inp = [int(x) for x in input().split()]
matriks2 = []
for x in range(inp[0]) :
    matriks2.append([int(x) for x in input().split()])
res = matmult(matriks1, matriks2)
print('\n'.join([''.join([str(item) + " " if item != row[-1] else str(item) for item in row]) for row in res]))
