import sys
prev = int(input())
curr = prev
res = 0
state = 1 #-1 untuk turun 1 untuk naik
for x in sys.stdin :
    x = int(x)
    if x > curr :
        if state < 0:
            state = 1
            res = abs(curr-prev) if res < abs(curr-prev) else res
            prev = curr
        curr = x
    elif x < curr :
        if state > 0:
            state = -1
            res = abs(curr-prev) if res < abs(curr-prev) else res
            prev = curr
        curr = x
    else :
        continue
res = abs(curr-prev) if res < abs(curr-prev) else res
print(res)
