jml = [int(x) for x in input().split()]
cat = []
for x in range(jml[0]) :
    cat.append(int(input()))
res, x, y = 1, 0, jml[1]
for x in range(0, jml[0], jml[1]) :
    res += max(cat[x:y]) + 1
    x,y = y + 1, min(y + jml[1], jml[0])
print(res)
