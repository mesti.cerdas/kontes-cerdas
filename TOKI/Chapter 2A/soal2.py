import sys
from collections import Counter

out = [int(x) for x in sys.stdin]
data = Counter(out)
res, count = data.most_common()[0]
for a,b in sorted(data.most_common()) :
    if b >= count :
        res = a
print(res)
