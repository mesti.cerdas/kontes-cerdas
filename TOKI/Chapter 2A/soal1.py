import sys

out = [x for x in sys.stdin]
for x in reversed(out) :
    print(x, end="")
