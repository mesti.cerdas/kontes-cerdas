inp = [int(n) for n in input().split()]
for x in range(1, inp[0]) :
    if x%inp[1] == 0 :
        print("*", end=" ")
    else :
        print(x, end=" ")
print(inp[0])
