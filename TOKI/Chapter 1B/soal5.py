import math

inp = [int(n) for n in input().split()]
print(int(math.fabs(inp[0] - inp[2]) + int(math.fabs(inp[1] - inp[3]))))