from functools import reduce
def factors(n):
    if n > 0 :
        return set(reduce(list.__add__, ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))
    else :
        return []

import sys
for x in sys.stdin :
    d = int(x)
    f = factors(d)
    if len(f) <= 2 and d > 1:
        print("YA")
    else :
        print("TIDAK")
