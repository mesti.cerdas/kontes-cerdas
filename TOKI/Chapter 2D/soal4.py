inp = [x for x in input().split()]
if inp[1] == '-' :
    print(int(inp[0]) - int(inp[2]))
elif inp[1] == '+' :
    print(int(inp[0]) + int(inp[2]))
elif inp[1] == '*' :
    print(int(inp[0]) * int(inp[2]))
elif inp[1] == '<' :
    out = "benar" if int(inp[0]) < int(inp[2]) else "salah"
    print(out)
elif inp[1] == '>' :
    out = "benar" if int(inp[0]) > int(inp[2]) else "salah"
    print(out)
elif inp[1] == '=' :
    out = "benar" if int(inp[0]) == int(inp[2]) else "salah"
    print(out)
