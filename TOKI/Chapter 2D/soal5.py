from statistics import mean
n = int(input())
data = []
for x in range(n) :
    data.append(float(input()))
print("%.2f" % min(data) + " " + "%.2f" % max(data) + " " + "%.2f" % mean(data))
