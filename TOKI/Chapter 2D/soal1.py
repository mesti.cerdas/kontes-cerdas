from functools import reduce
def factors(n):    
    return set(reduce(list.__add__, ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0)))

inp = int(input())
for x in range(inp) :
    num = int(input())
    if len(factors(num)) <= 4 :
        print("YA")
    else :
        print("TIDAK")
