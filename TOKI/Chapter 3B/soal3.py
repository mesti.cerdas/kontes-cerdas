def selection_sort(a):
    """
    select sort - time complexity: O(n^2),
    out performs 'bubble sort'
    """
    res = 0
    s = len(a)
    for i in range(0, s):
        m = i
        for j in range(i+1, s):
            if a[j] < a[m]:
                m = j
        if m != i :
            a[i], a[m] = a[m], a[i]
            res+=1
    return res

jml = int(input())
data = []
for x in range(jml) :
    data.append(int(input()))
print(selection_sort(data))
