inp = int(input())
data = [int(x) for x in input().split()]
data.sort()
if inp%2 != 0 :
    print(data[inp//2 - 1])
else :
    print("%.2f"%float((data[inp//2 - 1] + data[inp//2])/2))
