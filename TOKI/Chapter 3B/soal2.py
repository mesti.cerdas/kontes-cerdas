def insertion_sort(a):
    """
    insert sort -- time complexity: O(n^2), 
    generally out performs 'select sort'
    """
    s = len(a)
    res = 0
    for i in range(1, s):
        for j in range(i-1, -1, -1):
            if a[j]>a[j+1]:
                a[j+1], a[j] = a[j], a[j+1]
                res+=1
    return res

jml = int(input())
data = []
for x in range(jml) :
    data.append(int(input()))
print(insertion_sort(data))
