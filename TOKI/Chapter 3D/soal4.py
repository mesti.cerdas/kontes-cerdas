import statistics
n = int(input())
dt = []
for x in range(0,n):
    dt.append(float(input()))
sd = statistics.stdev(dt)
mn = statistics.mean(dt)
mi = min(dt)
mx = max(dt)
print("{:.2f} {:.2f} {:.2f} {:.2f}".format(mi, mx, mn, sd))
