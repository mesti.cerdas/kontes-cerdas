program refmat;
var
n,i,j : integer;
hor,ver,ide,dkr,dkn : boolean;
mat1, mat2 : array [0..76,0..76] of integer;

begin
	read(n,n);
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			read(mat1[i,j]);
		end;
	end;
	read(n,n);
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			read(mat2[i,j]);
		end;
	end;
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			if (mat1[i,j] = mat2[i,j]) then
				ide := true
			else
			begin
				ide := false;
				break;
			end;
		end;
		if not(mat1[i,j] = mat2[i,j]) then
			break;
	end;
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			if (mat1[i,j] = mat2[n-1-i,j]) then
				hor := true
			else
			begin
				hor := false;
				break;
			end;
		end;
		if not(mat1[i,j] = mat2[n-1-i,j]) then
			break;
	end;
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			if (mat1[i,j] = mat2[i,n-1-j]) then
				ver := true
			else
			begin
				ver := false;
				break;
			end;
		end;
		if not(mat1[i,j] = mat2[i,n-1-j]) then
			break;
	end;
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			if (mat1[i,j] = mat2[j,i]) then
				dkn := true
			else
			begin
				dkn := false;
				break;
			end;
		end;
		if not(mat1[i,j] = mat2[j,i]) then
			break;
	end;
	for i := 0 to (n-1) do
	begin
		for j := 0 to (n-1) do
		begin
			if (mat1[i,j] = mat2[n-1-j,n-1-i]) then
				dkr := true
			else
			begin
				dkr := false;
				break;
			end;
		end;
		if not(mat1[i,j] = mat2[n-1-j,n-1-i]) then
			break;
	end;
	if (ide = true) then
		writeln('identik')
	else if (hor = true) then
		writeln('horisontal')
	else if (ver = true) then
		writeln('vertikal')
	else if (dkr = true) then
		writeln('diagonal kiri bawah')
	else if (dkn = true) then
		writeln('diagonal kanan bawah')
	else
		writeln('tidak identik');
end.
