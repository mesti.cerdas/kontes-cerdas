#include<iostream>

using namespace std;

int main(){
	long long n, t; cin >> n;
	while(n--){
		cin >> t;
		long long int res = (t*t*t+t)/2;
		cout << res << endl;
	}
}
