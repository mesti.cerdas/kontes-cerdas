#include<iostream>

using namespace std;

long long gcd(long long a, long long b){
	while(b != 0){
		long long r = a%b;
		a = b;
		b = r;
	}
	return a;
}

int main(){
	//soal a/b + c/d = e/f
	long long a, b, c, d, e, f, g;
	cin >> a >> b >> c >> d;
	if(b == 0){
		e = c; f = d;
	} else if(d == 0){
		e = a; f = b;
	} else {
		g = gcd(b,d);
		b = b/g; d = d/g;
		a = a*d; c = c*b;
		f = b*d*g;
		e = a+c;
	}
	g = gcd(e,f);
	while(g != 1){
		g = gcd(e,f);
		e /= g;
		f /= g;
	}
	if(e == f) cout << 1 << endl; 
	else cout << e << " " << f << endl;
	return 0;
}