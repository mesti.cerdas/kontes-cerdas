#include<iostream>

using namespace std;

int main(){
	int n, t; cin >> n;
	while(n--){
		cin >> t;
		long long res = (t*t*t*t+t*t)/2;
		res /= t;
		cout << res << endl;
	}
}