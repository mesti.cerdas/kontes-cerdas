#include<iostream>
#include<vector>
#define MAX 1000000

using namespace std;

bool data[MAX]; //true prime
vector<int> prime;

int main(){
	data[2] = false;
	for (int i = 2; i < MAX; i++)
	{
		if(data[i]) continue;
		for (int j = 2*i; j < MAX; j+=i)
		{
			data[j] = true;
		}
		prime.push_back(i);
	}
	int t, n;
	cin >> t;
	while(t--){
		cin >> n;
		cout << prime[n-1] << endl;
	}
	return 0;
}