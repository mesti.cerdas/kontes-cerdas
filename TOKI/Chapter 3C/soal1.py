def get_prime_factors(number):
    """
    Return prime factor list for a given number
        number - an integer number
        Example: get_prime_factors(8) --> [2, 2, 2].
    """
    if number == 1:
        return []

    # We have to begin with 2 instead of 1 or 0
    # to avoid the calls infinite or the division by 0
    for i in range(2, number):
        # Get remainder and quotient
        rd, qt = divmod(number, i)
        if not qt: # if equal to zero
            return [i] + get_prime_factors(rd)

    return [number]

inp = int(input())
prim = get_prime_factors(inp)
d = {}
for x in prim :
    d[x] = d.get(x,0) + 1
for k,v in d.items():
    e = "^"+str(v) if (v > 1) else ""
    if(k != list(d.keys())[-1]) :
        print(str(k) + e, end=" x ")
    else :
        print(str(k) + e)
