#include<cstdio>

using namespace std;

int main(){
	int a; scanf("%d", &a);
	for(int i = 2; i <= a; i++){
		int count = 0;
		while(a%i == 0){
			count++;
			a/=i;
		}
		if(count != 0){
			printf("%d", i);
			if(count > 1) printf("^%d", count);
			if(a != 1) printf(" x "); else printf("\n");
		}
	}
	return 0;
}
