#include<iostream>

using namespace std;

int main(){
	long long n, t, dif, res = 1;
	cin >> t >> n;
	dif = n - t;
	while(res <= dif) res = res << 1;
	while(dif > 0){
		if(dif >= res){
			dif -= res;
			cout << res << endl;
		}
		res = res >> 1;
	}
	return 0;
}